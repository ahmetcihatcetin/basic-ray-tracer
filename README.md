# Basic Ray Tracer

Takes input as PHP format and renders image with backward rendering and gives the output image in ppm format. It has following features: 24bits per pixel, perspective projection, ambient-diffuse-specular shadings,shadows, reflections, optimized vector operations.
<br />To execute the program, first use make command then execute raytracer with sample inputs provided. After that, you may compare the output with the expected output from the sample_outputs folder.
<br />Sample usage:
<br />1)make
<br />2).\raytracer hw1_sample_scenes/dragon_lowres.xml 
<br />
<br />This program has been developed by Ahmet Cihat �etin and H�seyin Emre �eki� as a homework: semesterNo:20181-courseNo:5710477-Assignment#1