#ifndef __HW1__PARSER__
#define __HW1__PARSER__

#include <string>
#include <vector>

namespace parser
{
    //Notice that all the structures are as simple as possible
    //so that you are not enforced to adopt any style or design.
    struct Vec3f
    {
        float x, y, z;
        //My additions:
        Vec3f operator+ (const Vec3f &arg);
        Vec3f operator- (const Vec3f &arg);
        Vec3f operator* (float arg); //multiplication with a scalar by RHS
        //Vec3f operator* (float arg1, const Vec3f& arg2); //multiplication with a scalar by LHS
        Vec3f RGB_product(const Vec3f &arg);
        Vec3f operator/ (float arg);  //division by a scalar
        Vec3f& operator+= (const Vec3f &arg);
        float norm(); //the length of the vector
        float dot_product(const Vec3f &arg);
        Vec3f cross_product(const Vec3f &arg);
    };

    struct Vec3i
    {
        int x, y, z;
        //My additions
        Vec3i operator+ (const Vec3i &arg);
        Vec3i operator- (const Vec3i &arg);
        Vec3i operator* (int arg);  //multiplication with a scalar
        Vec3i operator/ (int arg);  //division by a scalar
        int dot_product(const Vec3i &arg);
        Vec3i cross_product(const Vec3i &arg);
    };

    struct Vec4f
    {
        float x, y, z, w;
        //My additions:
        Vec4f operator+ (const Vec4f &arg);
        Vec4f operator- (const Vec4f &arg);
        Vec4f operator* (float arg);  //multiplication with a scalar
        Vec4f operator/ (float arg);  //division by a scalar
    };

    struct Camera
    {
        Vec3f position;
        Vec3f gaze;
        Vec3f up;
        Vec4f near_plane;
        float near_distance;
        int image_width, image_height;
        std::string image_name;
    };

    struct PointLight
    {
        Vec3f position;
        Vec3f intensity;
    };

    struct Material
    {
        Vec3f ambient;
        Vec3f diffuse;
        Vec3f specular;
        Vec3f mirror;
        float phong_exponent;
    };

    struct Face
    {
        int v0_id;
        int v1_id;
        int v2_id;
    };

    struct Mesh
    {
        int material_id;
        std::vector<Face> faces;
    };

    struct Triangle
    {
        int material_id;
        Face indices;
    };

    struct Sphere
    {
        int material_id;
        int center_vertex_id;
        float radius;
    };

    struct Scene
    {
        //Data
        Vec3i background_color;
        float shadow_ray_epsilon;
        int max_recursion_depth;
        std::vector<Camera> cameras;
        Vec3f ambient_light;
        std::vector<PointLight> point_lights;
        std::vector<Material> materials;
        std::vector<Vec3f> vertex_data;
        std::vector<Mesh> meshes;
        std::vector<Triangle> triangles;
        std::vector<Sphere> spheres;

        //Functions
        void loadFromXml(const std::string& filepath);
    };
}



#endif
