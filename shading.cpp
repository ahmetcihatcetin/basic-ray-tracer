#include <iostream>
#include "parser.h"
#include "ppm.h"
#include <math.h>
#include <limits>
#include "intersection.h"


parser::Vec3f ambient_shading(parser::Vec3f k_ambient, parser::Vec3f ambient_light){
    parser::Vec3f color;
    color.x=ambient_light.x*k_ambient.x;
    color.y=ambient_light.y*k_ambient.y;
    color.z=ambient_light.z*k_ambient.z;
    return color;
}

parser::Vec3f diffuse_shading(parser::Vec3f inter_point, parser::Vec3f inter_normal, parser::Vec3f k_diffuse, parser::PointLight light_source){
    //std::cout<<"light_source_pos=("<<light_source.position.x<<","<<light_source.position.x<<","<<light_source.position.x<<")"<<" ";
    parser::Vec3f l=light_source.position-inter_point;  // the vector from inter_point to light_source_position!!!
    //std::cout<<"l=("<<l.x<<","<<l.y<<","<<l.z<<")"<<" ";
    //std::cout<<"l_norm="<<l.norm()<<std::endl;
    float l_norm=l.norm();
    l=l/l_norm;   // l is normalized!!!!

    float n_dotproduct_l=inter_normal.dot_product(l);
    float cos_teta_prime=fmaxf(0,n_dotproduct_l)/(l_norm*l_norm);
    //if (cos_teta_prime>0&&cos_teta_prime<0.001)
    //    std::cout<<"cos_teta_prime = "<<cos_teta_prime<<std::endl;
    parser::Vec3f color;
    color.x=k_diffuse.x*light_source.intensity.x*cos_teta_prime;
    color.y=k_diffuse.y*light_source.intensity.y*cos_teta_prime;
    color.z=k_diffuse.z*light_source.intensity.z*cos_teta_prime;
    //std::cout<<k_diffuse.y<<"|||"<<light_source.intensity.y<<std::endl;
    //if (color.x<255&&color.y<255&&color.z<255)
    //std::cout<<"k_diffuse="<<k_diffuse.x<<", cos_teta="<<cos_teta_prime<<" "<<std::endl;
    //std::cout<<"diffuse_color = "<<color.x<<","<<color.y<<","<<color.z<<std::endl;
    return color;
}

parser::Vec3f specular_shading(parser::Vec3f inter_point, parser::Vec3f inter_normal, parser::Vec3f k_specular, float phong_exp, parser::Vec3f direction, parser::PointLight light_source){
    parser::Vec3f v=direction*(-1);
    v=v/(v.norm());
    parser::Vec3f l=light_source.position-inter_point;  // the vector from inter_point to light_source_position!!!
    float l_norm=l.norm();
    l=l/(l.norm());

    parser::Vec3f v_plus_l=v+l;
    parser::Vec3f h=v_plus_l/(v_plus_l.norm()); //the vector h!
    float n_dotproduct_h=inter_normal.dot_product(h);
    float cos_teta_prime=fmaxf(0,n_dotproduct_h);
    cos_teta_prime=pow(cos_teta_prime,phong_exp)/(l_norm*l_norm);   //cos_teta_prime updated!
    //std::cout<<"exp="<<phong_exp<<", "<<"cos_teta_prime="<<cos_teta_prime<<" "<<std::endl;
    parser::Vec3f color;
    color.x=k_specular.x*light_source.intensity.x*cos_teta_prime;
    color.y=k_specular.y*light_source.intensity.y*cos_teta_prime;
    color.z=k_specular.z*light_source.intensity.z*cos_teta_prime;
    return color;
}

parser::Vec3f shade(parser::Scene& scene, parser::Vec3f e,parser::Vec3f d, int recursion_depth){

    //min t value:
    float t=std::numeric_limits<float>::max();     //min t value to be compared with other t values instead of comparing intersection points!!!!

    //It indicates whether the ray intersects or not:
    bool intersect_flag=false;
    //visible_flag=true;
    //Intersect the ray with the all objects:     ****************************************
    int inter_material_id;    //Material of the intersection point. UPDATE if necessary!

    parser::Vec3f inter_point;  //The intersection point. UPDATE if necessary!
    parser::Vec3f inter_normal; //The normal vector of the plane which is containing the intersection point.

    //Iteration 4: Iterate over triangles:
    for(std::vector<parser::Triangle>::iterator it2=scene.triangles.begin(); it2!=scene.triangles.end(); it2++)
    {
        //indices a,b and c of the current triangle:
        parser::Vec3f a=scene.vertex_data[it2->indices.v0_id-1];
        parser::Vec3f b=scene.vertex_data[it2->indices.v1_id-1];
        parser::Vec3f c=scene.vertex_data[it2->indices.v2_id-1];
        //intersect the ray with the current triangle:
        bool flag=intersect_face(e, d, a, b, c, t);
        //std::cout<<"t for a triangle: "<<t<<std::endl;
        if (flag)
        {
            intersect_flag=true;
            inter_material_id=it2->material_id;    //UPDATE inter_material_id (t is already UPDATED)
            inter_normal=(b-a).cross_product(c-a);    //UPDATE normal_vector
        }
    }

    //Iteration 5: Iterate over meshes:
    for(std::vector<parser::Mesh>::iterator it3=scene.meshes.begin(); it3!=scene.meshes.end(); it3++)
    {
        //Iteration 6: Iterate over faces:
        for(std::vector<parser::Face>::iterator it4=it3->faces.begin(); it4!=it3->faces.end(); it4++)
        {
            //indices a,b and c of the current face:
            parser::Vec3f a=scene.vertex_data[it4->v0_id-1];
            parser::Vec3f b=scene.vertex_data[it4->v1_id-1];
            parser::Vec3f c=scene.vertex_data[it4->v2_id-1];
            //intersect the ray with the current face:
            if (intersect_face(e, d, a, b, c, t))
            {
                intersect_flag=true;
                inter_material_id=it3->material_id;    //UPDATE inter_material_id (t is already UPDATED)
                inter_normal=(b-a).cross_product(c-a);    //UPDATE normal vector
            }
        }
    }

    //Iteration 7: Iterate over spheres:
    for(std::vector<parser::Sphere>::iterator it5=scene.spheres.begin(); it5!=scene.spheres.end(); it5++)
    {
        //center c and radius R:
        parser::Vec3f c=scene.vertex_data[it5->center_vertex_id-1];   //center of the current sphere!
        float R=it5->radius;    //radius of the current sphere!
        //intersect the ray with the current sphere:
        if (intersect_sphere(e, d, c, R, t))
        {
            intersect_flag=true;
            inter_material_id=it5->material_id;    //UPDATE inter_material_id (t is already UPDATED)
            inter_point=e+d*t;  //The intersection point is UPDATED!!!!!!!!!!!!!
            inter_normal=(inter_point-c); //UPDATE normal vector
        }
    }

    //Now we have the inter point with the lowest t value.

    //Now calculate the pixel colors: *******************************************************************************
    parser::Vec3f image_color;

    //Intialize the image_color with background_color:
    image_color.x=scene.background_color.x;
    image_color.y=scene.background_color.y;
    image_color.z=scene.background_color.z;






    if (intersect_flag)
    {

        inter_point=e+d*t;  //The intersection point !!!!!!!!!!!!!!
        parser::Material inter_material=scene.materials[inter_material_id-1];

        inter_normal=inter_normal/inter_normal.norm();

                    //Ambient Shading:

                    image_color=ambient_shading(inter_material.ambient,scene.ambient_light);

                    bool visible_flag=true;
                    //Diffuse Shading & Spacular Shading:

                    //Iteration 8: Iterate over point lights:
                    for (std::vector<parser::PointLight>::iterator it6=scene.point_lights.begin(); it6!=scene.point_lights.end(); it6++)
                    {

                        visible_flag=true;
                        //check whether the intersection point is in the shadow of the current light (i.e compare with other objects):
                        parser::Vec3f l=it6->position-inter_point; // l value for shadow rays
                        //direction vector to the lght point from the intersection point
                        parser::Vec3f new_interpoint=inter_point+(l*(scene.shadow_ray_epsilon));
                        float t1=1;

                        //Iteration 9: iterate over spheres:
                        for(std::vector<parser::Sphere>::iterator it7=scene.spheres.begin(); it7!=scene.spheres.end(); it7++)
                        {
                            //center c and radius R:
                            parser::Vec3f c=scene.vertex_data[it7->center_vertex_id-1];   //center of the current sphere!
                            float R=it7->radius;    //radius of the current sphere!
                            //intersect the ray with the current sphere:
                            if (intersect_sphere(new_interpoint, l, c, R, t1))
                            {
                                visible_flag=false;
                                break;
                            }
                        }
                        if(!visible_flag)
                            continue;

                        //Iteration 10: iterate over triangles:
                        for(std::vector<parser::Triangle>::iterator it8=scene.triangles.begin(); it8!=scene.triangles.end(); it8++)
                        {
                            //indices a,b and c of the current triangle:
                            parser::Vec3f a=scene.vertex_data[it8->indices.v0_id-1];
                            parser::Vec3f b=scene.vertex_data[it8->indices.v1_id-1];
                            parser::Vec3f c=scene.vertex_data[it8->indices.v2_id-1];
                            //intersect the ray with the current triangle:

                            //std::cout<<"t for a triangle: "<<t<<std::endl;
                            if (intersect_face(new_interpoint, l, a, b, c, t1))
                            {
                                    visible_flag=false;
                                    break;
                            }
                        }
                        if(!visible_flag)
                            continue;

                        //Iteration 11: iterate over meshes:
                        for(std::vector<parser::Mesh>::iterator it9=scene.meshes.begin(); it9!=scene.meshes.end(); it9++)
                        {
                            //Iteration 12: Iterate over faces:
                            for(std::vector<parser::Face>::iterator it10=it9->faces.begin(); it10!=it9->faces.end(); it10++)
                            {
                                //indices a,b and c of the current face:
                                parser::Vec3f a=scene.vertex_data[it10->v0_id-1];
                                parser::Vec3f b=scene.vertex_data[it10->v1_id-1];
                                parser::Vec3f c=scene.vertex_data[it10->v2_id-1];
                                //intersect the ray with the current face:
                                if (intersect_face(new_interpoint, l, a, b, c, t1))
                                {
                                    visible_flag=false;
                                    break;
                                }
                            }
                            if(!visible_flag)
                                break;

                        }
                        if(!visible_flag)
                            continue;

                            image_color=image_color+diffuse_shading(inter_point, inter_normal, inter_material.diffuse, *it6);

                            image_color=image_color+specular_shading(inter_point, inter_normal, inter_material.specular, inter_material.phong_exponent, d, *it6);

                    }

                    //visible_flag=true;
                    //at this point shadow and shading has finished!!!!!!!!

                    //Now it's reflection time:
                    if (recursion_depth<scene.max_recursion_depth)
                    {
                        if (inter_material.mirror.x!=0||inter_material.mirror.y!=0||inter_material.mirror.z!=0)
                        {
                            parser::Vec3f d_unit=d/(d.norm());
                            parser::Vec3f d_reflection=d_unit-inter_normal*2*(inter_normal.dot_product(d_unit));
                            d_reflection=d_reflection/(d_reflection.norm());
                            parser::Vec3f new_inter_point=inter_point+(d_reflection*(scene.shadow_ray_epsilon));
                            image_color=image_color+inter_material.mirror.RGB_product(shade(scene, new_inter_point, d_reflection, ++recursion_depth));

                        }
                    }
    }
    return image_color;
}
