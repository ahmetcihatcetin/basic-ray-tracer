#ifndef __HW1__SHADING__
#define __HW1__SHADING__

#include <iostream>
#include "parser.h"
#include "ppm.h"
#include <math.h>
#include <limits>
#include "intersection.h"

parser::Vec3f ambient_shading(parser::Vec3f k_ambient, parser::Vec3f ambient_light);

parser::Vec3f diffuse_shading(parser::Vec3f inter_point, parser::Vec3f inter_normal, parser::Vec3f k_diffuse, parser::PointLight light_source);

parser::Vec3f specular_shading(parser::Vec3f inter_point, parser::Vec3f inter_normal, parser::Vec3f k_specular, float phong_exp, parser::Vec3f direction, parser::PointLight light_source);

parser::Vec3f shade(parser::Scene& scene, parser::Vec3f e,parser::Vec3f d, int recursion_depth);



#endif
