#ifndef __HW1__INTERSECTION__
#define __HW1__INTERSECTION__

#include <iostream>
#include "parser.h"
#include "ppm.h"
#include <math.h>


    //Intersect a ray with a triangle:
    bool intersect_face(parser::Vec3f e, parser::Vec3f d, parser::Vec3f a, parser::Vec3f b, parser::Vec3f c, float &t_max);

    //intersect a ray with a sphere:
    bool intersect_sphere(parser::Vec3f e, parser::Vec3f d, parser::Vec3f c, float R, float &t_max);


#endif