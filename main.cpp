#include <iostream>
#include "parser.h"
#include "ppm.h"
#include <math.h>
#include "intersection.h"
#include "shading.h"

typedef unsigned char RGB[3];



int main(int argc, char* argv[])
{

    // Sample usage for reading an XML scene file
    parser::Scene scene;

    scene.loadFromXml(argv[1]);

    //My additions:

    //Iteration 1: Iterate over cameras:
    for (std::vector<parser::Camera>::iterator it1=scene.cameras.begin(); it1!=scene.cameras.end(); it1++)
    {


        //Step1: Calculate w and u vectors:
        parser::Vec3f e=it1->position;  // eye
        parser::Vec3f v=it1->up;    // v vector(up vector)
        parser::Vec3f w=it1->gaze*(-1); // w vector
        parser::Vec3f u=v.cross_product(w); // u vector

        //Step2: Find point q which is the starting point of image(near) plane:
        float left=it1->near_plane.x;   //left value of near plane
        float right=it1->near_plane.y;  //right value of near plane
        float bottom=it1->near_plane.z; //bottom value of near plane
        float top=it1->near_plane.w;    //top value of near plane

        parser::Vec3f q=e+it1->gaze*it1->near_distance+u*left+v*top;    //point q which is the starting point of image(near) plane

        //Step3: Find pixel width and pixel height:
        float pixel_width=(right-left)/it1->image_width;    //pixel width
        float pixel_height=(top-bottom)/it1->image_height;   //pixel height

        //Char5rre:
        unsigned char* image = new unsigned char [it1->image_width * it1->image_height * 3];
        //Index of the pixel to be placed in char array:
        int pixel_index=0;  //UPDATE EVERYTIME when setting the pixel color of the pixel index: image[pixel_index++] = ********; !!!!!!!!!!!!!!!!!!!!!

        //Iteration 2: Iterate over j:
        for (int j=0; j<it1->image_height; j++)
        {
            //Iteration 3: Iterate over i:
            for (int i=0; i<it1->image_width; i++)
            {

                //Step4: Find point s which is the position of the pixel. We're senging our ray from point e to s:
                parser::Vec3f s=q+u*(pixel_width*(i+0.5))+v*(-1*pixel_height*(j+0.5));  //point s !!!!!

                //Step5: Find d which is the direction vector of the ray:
                parser::Vec3f d=s-e; // d (direction vector) !!!!!!!!




                int recursion_depth=0;

				parser::Vec3f pixel_color=shade(scene,e,d,recursion_depth);

                if (pixel_color.x>255)
                        pixel_color.x=255;
                    if (pixel_color.y>255)
                        pixel_color.y=255;
                    if (pixel_color.z>255)
                        pixel_color.z=255;

                    image[pixel_index++]=pixel_color.x;
                    image[pixel_index++]=pixel_color.y;
                    image[pixel_index++]=pixel_color.z;



            }


         }
            write_ppm(it1->image_name.c_str(), image, it1->image_width, it1->image_height);
      }



}
