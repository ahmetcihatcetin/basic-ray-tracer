#include <iostream>
#include "parser.h"
#include "ppm.h"
#include <cmath>
#include "intersection.h"

//Intersect a ray with a triangle
bool intersect_face(parser::Vec3f e, parser::Vec3f d, parser::Vec3f a, parser::Vec3f b, parser::Vec3f c, float &t_max)
{
    //compute t:
    float a_x_minus_b_x=a.x-b.x;
    float a_y_minus_e_y=a.y-e.y;
    float a_y_minus_b_y=a.y-b.y;
    float a_x_minus_e_x=a.x-e.x;
    float ak_minus_jb=a_x_minus_b_x*a_y_minus_e_y-a_y_minus_b_y*a_x_minus_e_x;
    float a_z_minus_b_z=a.z-b.z;
    float a_z_minus_e_z=a.z-e.z;
    float jc_minus_al=a_z_minus_b_z*a_x_minus_e_x-a_x_minus_b_x*a_z_minus_e_z;
    float bl_minus_kc=a_y_minus_b_y*a_z_minus_e_z-a_z_minus_b_z*a_y_minus_e_y;
    float a_y_minus_c_y=a.y-c.y;
    float a_z_minus_c_z=a.z-c.z;
    float ei_minus_hf=d.z*a_y_minus_c_y-d.y*a_z_minus_c_z;
    float a_x_minus_c_x=a.x-c.x;
    float gf_minus_di=d.x*a_z_minus_c_z-d.z*a_x_minus_c_x;
    float dh_minus_eg=d.y*a_x_minus_c_x-d.x*a_y_minus_c_y;
    float M=a_x_minus_b_x*ei_minus_hf+a_y_minus_b_y*gf_minus_di+a_z_minus_b_z*dh_minus_eg;
    //std::cout<<"ak_minus_jb for a face = "<<ak_minus_jb<<std::endl;

    //std::cout<<"M for a face = "<<M<<std::endl;
    //std::cout<<"jc_minus_al for a face = "<<jc_minus_al<<std::endl;
    //compute t:
    float t=(a_z_minus_c_z*ak_minus_jb+a_y_minus_c_y*jc_minus_al+a_x_minus_c_x*bl_minus_kc)/(-1*M); //t value !!!!!!!!!!!!!!!!!!!
    //std::cout<<"t for a face = "<<t<<std::endl;

    if (t<=0 || t>=t_max)
        return false;

    //compute gamma:
    float gamma=(d.z*ak_minus_jb+d.y*jc_minus_al+d.x*bl_minus_kc)/M;

    if (gamma<0 || gamma>1)
        return false;

    //compute beta:
    float beta=(a_x_minus_e_x*ei_minus_hf+a_y_minus_e_y*gf_minus_di+a_z_minus_e_z*dh_minus_eg)/M;

    if (beta<0 || beta>1-gamma)
        return false;

    //If we've reached this point, then it means we've found our new t and we must UPDATE it!!!!!!
    t_max=t;
    return true;
}


//intersect a ray with a sphere:
bool intersect_sphere(parser::Vec3f e, parser::Vec3f d, parser::Vec3f c, float R, float &t_max)
{
    parser::Vec3f e_minus_c=e-c;
    float d_dotproduct_e_minus_c= d.dot_product(e_minus_c);
    float d_dotproduct_d=d.dot_product(d);
    //Compute delta:
    float delta=d_dotproduct_e_minus_c*d_dotproduct_e_minus_c-d_dotproduct_d*((e_minus_c.dot_product(e_minus_c))-R*R);

    if (delta<0)
        return false;
    
    else if (delta==0)
    {
        //Compute t:
        float t= (-1*d_dotproduct_e_minus_c)/d_dotproduct_d;

        if (t<=0 || t>=t_max)
            return false;
        else
        {
            t_max=t;    //UPDATE t_max !!!!!
            return true;
        }
    }

    else
    {
        //Compute t1 and t2:
        float square_root_delta=std::sqrt(delta);
        float t_1=(-1*d_dotproduct_e_minus_c+square_root_delta)/d_dotproduct_d;
        float t_2=(-1*d_dotproduct_e_minus_c-square_root_delta)/d_dotproduct_d;

        if (t_1 <= 0)
            return false;
        else if (t_2<0 && t_1>0 )
        {
            if (t_1<t_max)
            {
                t_max=t_1;
                return true;
            }
            return false;
        }
        else
        {
            if (t_2<t_max)
            {
                t_max=t_2;
                return true;
            }
            return false;
        }
    }
}